<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\User;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $post = factory(App\Post::class, 3)->create();
        DB::table('posts')->insert([
            'user_id' => '4',
            'caption' => 'Hello, This is my first post!',
            'image' => 'img/foto1.jpg',
        ]);
        //
        DB::table('posts')->insert([
            'user_id' => '4',
            'caption' => 'Hello, This is my second post!',
            'image' => 'img/foto2.jpg',
        ]);

        DB::table('posts')->insert([
            'user_id' => '4',
            'caption' => 'Hello, This is my third post!',
            'image' => 'img/foto3.jpg',
        ]);
    }
}
