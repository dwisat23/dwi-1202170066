@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @foreach($posts as $post)
            <div class="card">

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-1"> 

                            <img src="img/{{ \App\User::where(['id' => $post->user_id])->first()->avatar}}" style="border-radius: 50%; width: 30px; height: 30px;">

                        </div>
                        <div class="col-md-11">
                            {{ \App\User::where(['id' => $post->user_id])->first()->name}}
                        </div>
                    </div>                
                </div>

                <div class="card-body2">
                   <center>
                    <a href="{{route('detail', $post->id)}}">
                        <img src="{{ str_replace('public/','', $post->image) }}" style="position: relative; width: 100%;">
                    </a>
                </center>
            </div>

            <div class="card-header">
                <div class="container">
                  <div class="row justify-content-start">
                    <div class="col-1">
                        <form method="POST" action="">
                            @csrf
                            <input type="hidden" name="post_id" value="{{$post->id}}">
                            <button type="submit" class="btn"><i class="" style="font-size: 21px;"></i></button>
                        </form>
                    </div>
                    <div class="col-1">
                        <a href="{{route('detail', $post->id)}}"><i class="" style="font-size: 21px;"></i></a>
                    </div>
                </div>
            </div>

            <div style="padding-top: 10px;">
                <b>Likes</b>
            </div>

            <div style="padding-top: 10px;">
                <b>{{ \App\User::where(['id' => $post->user_id])->first()->name }}</b><br>
                {{$post->caption}}<br>

                <hr>

                @foreach($post->comments as $comment)
                <b>{{ $comment->user->name}}</b>
                {{ $comment->comment }}
                <br>
                @endforeach
                
            </div>

            <div style="padding-top: 10px;">

                <form method="POST" action="{{route('addkomen')}}">
                    @csrf
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" placeholder="Text" aria-label="Recipient's username" aria-describedby="button-addon2" name="comment">

                      <input type="hidden" name="user_id" value=" {{ Auth::user()->id }}">

                      <input type="hidden" name="post_id" value="{{$post->id}}">

                      <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Post</button>
                    </div>
                </div>
            </form>

        </div>



    </div>
</div>
<br>
<br>
@endforeach

</div>
</div>
</div>
@endsection
