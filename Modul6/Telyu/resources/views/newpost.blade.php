@extends('layouts.app')

@section('content')

<div class="container" style="padding-top: 20px;">


        <div class="container" style="padding: 0 180px 0 180px;">
            <h3>Add New Post</h3>

            <form method="POST" action="{{route('newpostaction')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="caption">Post Caption</label>
                    <input name="caption" type="text" class="form-control" id="caption" placeholder="">
                </div>
                <div class="form-group">
                    <label for="image">Post Image</label>
                    <input name="foto" type="file" class="form-control-file" id="foto">
                </div>

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="likes" value="0">

                <button type="submit" class="btn btn-primary">Add New Post</button>
            </form>

        </div>

</div>
@endsection
