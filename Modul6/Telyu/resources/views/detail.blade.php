@extends('layouts.app')

@section('content')
<div class="container">

   @foreach($post as $ps)
   <div class="row">

    <div class="col-md-6">
        <div class="card">
            <div class="card-body2">
                <img src="../../{{  $ps->image }}" style="position: relative; width: 100%;">
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-1"> 
                <img src="../../img/{{ \App\User::where(['id' => $ps->user_id])->first()->avatar}}" style="border-radius: 50%; width: 30px; height: 30px;">
            </div>
            <div class="col-md-11">
                {{ \App\User::where(['id' => $ps->user_id])->first()->name}}
            </div>
        </div>

        <hr>

        <div style="padding-top: 10px;">
            <b>{{ \App\User::where(['id' => $ps->user_id])->first()->name }}</b>
            {{$ps->caption}}
            <br>
            <hr>
            {{-- <b>{{ \App\Coment::where(['id' => $ps->post_id])->first()->email}}</b> --}}

            @foreach($comment as $com)
            <b>{{ \App\User::where(['id' => $com->user_id])->value('name')}}</b>
            {{ $com->comment}}<br>
            @endforeach
        </div>

        <hr>

        <div class="container">
          <div class="row justify-content-start">
            <div class="col-1">
                <form method="POST" action="{{route('likes')}}">
                    @csrf
                    <input type="hidden" name="post_id" value="{{$ps->id}}">
                    <button type="submit" class="btn"><i class="" style="font-size: 21px;"></i></button>
                </form>
            </div>
            
        </div>

        <div style="padding-top: 10px;">
            <b>{{$ps->likes}} Likes</b>
        </div>

        <div style="padding-top: 10px;">

            <form method="POST" action="{{route('addkomen2')}}">
                @csrf
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Text" aria-label="Recipient's username" aria-describedby="button-addon2" name="comment">

                  <input type="hidden" name="user_id" value=" {{ Auth::user()->id }}">

                  <input type="hidden" name="post_id" value="{{$ps->id}}">

              </div>
          </form>

      </div>

  </div>

</div>
</div>
@endforeach
</div>
@endsection
