<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('users')->insert([
            'name' => 'Muh. Dwi Satya',
            'email' => 'dwi'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'img/foto dwi.jpg',
        ]);
        //
    }
}