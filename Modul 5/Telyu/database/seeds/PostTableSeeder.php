<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Hello, Ini postingan pertama saya',
            'image' => 'img/foto1.jpg',
        ]);
        //
        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Hello, Ini postingen ke-2 saya',
            'image' => 'img/foto2.jpg',
        ]);

        DB::table('posts')->insert([
            'user_id' => '1',
            'caption' => 'Hello, Ini postingan ke-3 saya',
            'image' => 'img/foto3.jpg',
        ]);
    }
}