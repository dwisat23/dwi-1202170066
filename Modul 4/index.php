<!doctype html>
<html lang="en">
  <head>
    <title>WAD Store</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?php
include_once("koneksi.php");
session_start();
$result = mysqli_query($mysqli, "SELECT * FROM users_table ORDER BY id DESC");
?>
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <link href="pricing.css" rel="stylesheet">
  </head>
  <body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal"><img src="img/EAD.png" style="width:160px;height:40px;"></h5>
  <nav class="my-2 my-md-0 mr-md-3">
    
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Register</button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModallogin">
  Login</button>
  </nav>
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4" style="color:rgb(220, 20, 60)"><b>Hello Coders</b></h1>
  <p class="lead" style="color:rgb(220, 20, 60)"><b>Welcome to our store, please take a look for the product you might buy.</b></p>
</div>
<div class="container">
  <div class="card-deck mb-3 text-left">
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><img src="img/c.png" style="width:245px;height:200px;"></h4>
      </div>
      <div class="card-body">
        <h4 class="#"><b>Learning Basic Programming</b></h4>
        <p>Rp.210.000,-</p>
        <br>
        <p>Membuat anda kayah dengan membeli ini (jangan harap!!!!)</p>
        <input type="Submit" name="buy" value="BUY" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#belomlogin">
      </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><img src="img/j.png" style="width:245px;height:200px;"></h4>
      </div>
      <div class="card-body">
        <h4 class="#"><b>Java Programming</b></h4>
        <br>
        <p>Rp.240.000,-</p>
        <br>
        <p>Membuat anda bisa membuat game (Mimpiiiii)</p>
        <input type="Submit" name="buy" value="BUY" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#belomlogin">
      </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><img src="img/gundala.jpg" style="width:245px;height:200px;"></h4>
      </div>
      <div class="card-body">
        <h4 class="#"><b>Gundala Programming</b></h4>
        <br>
        <p>Rp.255.000,-</p>
        <br>
        <p>Membuat anda lebih tahu menahu tentang apa saja yang anda inginkan :)))</p>
        <input type="Submit" name="buy" value="BUY" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#belomlogin">
      </div>
    </div>
  </div>


  <footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
        <center><small class="d-block mb-3 text-muted">&copy; EAD STORE</small></center>
      </div>
      </div>
  </footer>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Register</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form action="index.php" method="post" name="form1">
     <div class="form-group">
      <label for="exampleInputEmail1">Fullname</label>
      <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Fullname" required>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Username</label>
      <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Phone Number</label>
      <input type="number" class="form-control" id="mobile_number" name="mobile_number" placeholder="Phone" required>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
    </div>
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <input type="Submit" name="Submit" value="Register" class="btn btn-primary">
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModallogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="login.php" method="post" name="formlogin">
     
    <div class="form-group">
      <label for="exampleInputEmail1">Username</label>
      <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
    </div>
    
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password" required>
    </div>
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="Submit" name="Login" value="Login" class="btn btn-primary">Login</button>

      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="belomlogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="alert alert-danger" role="alert">
  Anda harus LOGIN terlebih dahulu untuk melakukan pembelian :(
</div>
      </div>
      <div class="modal-body">
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <input type="Submit" name="Login" value="Login Here" class="btn btn-primary" data-toggle="modal" data-target="#exampleModallogin" data-dismiss="modal">

      </div>
      </div>
  </div>
</div>
<?php

    if(isset($_POST['Submit'])) {
        $fullname = $_POST['fullname'];
        $email = $_POST['email'];
        $username = $_POST['username'];
        $mobile_number = $_POST['mobile_number'];
        $password = $_POST['password'];


        
        include_once("koneksi.php");

        
        $result = mysqli_query($mysqli, "INSERT INTO users_table(fullname,email,username,mobile_number,password) VALUES('$fullname','$email','$username','$mobile_number','$password')");

        
        echo "User added successfully. <a href='index.php'></a>";
    }
    ?>

    


</body>
</html>
